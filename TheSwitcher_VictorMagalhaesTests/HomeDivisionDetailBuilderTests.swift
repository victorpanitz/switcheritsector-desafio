//
//  ViewController.swift
//  TheSwitcher_VictorMagalhaes
//
//  Created by Victor on 18/12/18.
//  Copyright © 2018 Victor. All rights reserved.
//

import Quick
import Nimble

@testable import TheSwitcher_VictorMagalhaes

class HomeDivisionDetailBuilderTests: QuickSpec {
    
    override func spec() {
        
        var builder: HomeDivisionDetailBuilder?
        
        describe("HomeDivisionDetailBuilder") {
            
            describe("quando `makeViewController` for acionado"){
                builder = HomeDivisionDetailBuilder(status: true, division: "lorem ipsum")
                
                expect(builder?.makeViewController() is HomeDivisionDetailViewController) == true
            }
        
        }
    }
}
