//
//  ViewController.swift
//  TheSwitcher_VictorMagalhaes
//
//  Created by Victor on 18/12/18.
//  Copyright © 2018 Victor. All rights reserved.
//

import Quick
import Nimble

@testable import TheSwitcher_VictorMagalhaes

class HomeDivisionsListPresenterTests: QuickSpec {
    
    override func spec() {
        
        var presenter: HomeDivisionsListPresenter?
        var view: HomeDivisionsListViewSpy?
        var router: HomeDivisionsListRouterSpy?
        
        describe("HomeDivisionsListPresenter") {
            
            beforeEach {
                view = HomeDivisionsListViewSpy()
                router = HomeDivisionsListRouterSpy()
                presenter = HomeDivisionsListPresenter(router: router!)
                
                presenter?.attachView(view: view!)
            }
            
            describe("quando a view for anexada") {
                
                it("então deverá configurar a lista de divisões da casa") {
                    expect(view?.setHomeDivisionsCalled) == true
                    expect(view?.homeDivisionsPassed) == HomeDivision.dummies
                    expect(view?.homeDivisionsPassed?.count) == 4
                }
                
            }
            
            describe("quando um ambiente for selecionado") {
                
                it("então deverá navegar para a cena de detalhes do ambiente"){
                    presenter?.homeDivisionTriggered(index: 0)
                    
                    expect(router?.navigateToHomeDivisionDetailsCalled) == true
                    expect(router?.homeLightStatusPassed) == false
                    expect(router?.divisionPassed) == "Kitchen"

                }
            }
        
        }
    }
}

class HomeDivisionsListViewSpy: HomeDivisionsListView {
    var setHomeDivisionsCalled: Bool?
    var homeDivisionsPassed: [HomeDivision]?
    var divisionPassed: String?
    
    func setHomeDivisions(divisions: [HomeDivision]) {
        setHomeDivisionsCalled = true
        homeDivisionsPassed = divisions
    }
}

class HomeDivisionsListRouterSpy: HomeDivisionsListRoutering {
    var navigateToHomeDivisionDetailsCalled: Bool?
    var homeLightStatusPassed: Bool?
    var divisionPassed: String?
    
    func navigateToHomeDivisionDetails(division: String, status: Bool) {
        navigateToHomeDivisionDetailsCalled = true
        homeLightStatusPassed = status
        divisionPassed = division
    }
}

extension HomeDivision {
    static var dummies: [HomeDivision] {
        return [
            HomeDivision(title: "Kitchen", lampStatus: false),
            HomeDivision(title: "Living Room", lampStatus: false),
            HomeDivision(title: "Master Bedroom", lampStatus: false),
            HomeDivision(title: "Guest's bedroom", lampStatus: false)
        ]
    }
}
