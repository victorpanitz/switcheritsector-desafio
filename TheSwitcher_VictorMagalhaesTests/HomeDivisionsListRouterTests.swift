//
//  ViewController.swift
//  TheSwitcher_VictorMagalhaes
//
//  Created by Victor on 18/12/18.
//  Copyright © 2018 Victor. All rights reserved.
//

import Quick
import Nimble

@testable import TheSwitcher_VictorMagalhaes

class HomeDivisionsListRouterTests: QuickSpec {
    
    override func spec() {
        
        var router: HomeDivisionsListRouter?
        
        describe("HomeDivisionsListRouter") {
            
            describe("quando `makeViewController` for acionado"){
                router = HomeDivisionsListRouter()
                
                expect(router?.makeViewController() is UINavigationController) == true
            }
        }
    }
}
