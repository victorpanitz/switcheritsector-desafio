//
//  ViewController.swift
//  TheSwitcher_VictorMagalhaes
//
//  Created by Victor on 18/12/18.
//  Copyright © 2018 Victor. All rights reserved.
//

import Quick
import Nimble

@testable import TheSwitcher_VictorMagalhaes

class HomeDivisionDetailPresenterTests: QuickSpec {
    
    override func spec() {
        
        var presenter: HomeDivisionDetailPresenter?
        var view: HomeDivisionDetailViewSpy?
        
        describe("HomeDivisionDetailPresenter") {
            
            func setup(status: Bool) {
                view = HomeDivisionDetailViewSpy()
                
                presenter = HomeDivisionDetailPresenter(status: status, division: "Kitchen")
                
                presenter?.attachView(view: view!)
            }
            
            beforeEach {
                setup(status: true)
            }
            
            describe("quando a view for anexada") {
                
                it("então deverá configurar a descrição do status da cena") {
                    expect(view?.statusDescriptionPassed) == "Your Kitchen light is"
                    expect(view?.setStatusDescriptionCalled) == true
                }
                
                context(" e possuir status `true`") {
                    
                    it("então deverá configurar o título como status de luz acesa"){
                        expect(view?.statusTitlePassed) == "ON"
                        expect(view?.setStatusTitleCalled) == true
                    }
                    
                    it("então deverá configurar a imagem como status de luz acesa"){
                        expect(view?.statusImagePassed) == "ic_on"
                        expect(view?.setImageCalled) == true
                    }

                }
                
                context(" e possuir status `false`") {
                    
                    beforeEach {
                        setup(status: false)
                    }
                    
                    it("então deverá configurar o título como status de luz apagada"){
                        expect(view?.statusTitlePassed) == "OFF"
                        expect(view?.setStatusTitleCalled) == true
                    }
                    
                    it("então deverá configurar a imagem como status de luz apagada"){
                        expect(view?.statusImagePassed) == "ic_off"
                        expect(view?.setImageCalled) == true
                    }
                }
            }
        }
    }
}

class HomeDivisionDetailViewSpy: HomeDivisionDetailView {
    var statusDescriptionPassed: String?
    var setStatusDescriptionCalled: Bool?
    var statusTitlePassed: String?
    var setStatusTitleCalled: Bool?
    var statusImagePassed: String?
    var setImageCalled: Bool?
 
    func setDescription(_ text: String) {
        statusDescriptionPassed = text
        setStatusDescriptionCalled = true
    }
    
    func setStatus(_ text: String) {
        statusTitlePassed = text
        setStatusTitleCalled = true
    }
    
    func setImage(_ text: String) {
        statusImagePassed = text
        setImageCalled = true
    }
}
