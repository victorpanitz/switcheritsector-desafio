//
//  AppDelegate.swift
//  TheSwitcher_VictorMagalhaes
//
//  Created by Victor on 18/12/18.
//  Copyright © 2018 Victor. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        setupRootViewController()
        
        return true
    }
    
    fileprivate func setupRootViewController() {
        window = UIWindow(frame: UIScreen.main.bounds)
        let router = HomeDivisionsListRouter()
        window?.rootViewController = router.makeViewController()
        window?.makeKeyAndVisible()
    }
}

