//
//  ViewController.swift
//  TheSwitcher_VictorMagalhaes
//
//  Created by Victor on 18/12/18.
//  Copyright © 2018 Victor. All rights reserved.
//

protocol HomeDivisionsListRoutering: class {
    func navigateToHomeDivisionDetails(division: String, status: Bool)
}

protocol HomeDivisionsListView: class {
    func setHomeDivisions(divisions: [HomeDivision])
}
