//
//  ViewController.swift
//  TheSwitcher_VictorMagalhaes
//
//  Created by Victor on 18/12/18.
//  Copyright © 2018 Victor. All rights reserved.
//

import UIKit

class HomeDivisionsListViewController: UIViewController {

    private let presenter: HomeDivisionsListPresenter
    private let tableView = UITableView()
    private lazy var dataSource = [HomeDivision]()
    
    init(presenter: HomeDivisionsListPresenter) {
        self.presenter = presenter
        
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { return nil }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationBar()
        setupTableView()
        
        presenter.attachView(view: self)
    }
    
    private func setupNavigationBar() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = .green
        navigationController?.navigationBar.topItem?.title = "Switcher App"
        
        let backItem = UIBarButtonItem()
        backItem.tintColor = .black
        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableFooterView = UIView()

        tableView.register(HomeDivisionCell.self, forCellReuseIdentifier: "reuse")
        
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
                tableView.widthAnchor.constraint(equalTo: view.widthAnchor),
                tableView.heightAnchor.constraint(equalTo: view.heightAnchor)
            ])
    }

}

extension HomeDivisionsListViewController: HomeDivisionsListView {
    func setHomeDivisions(divisions: [HomeDivision]) {
        dataSource = divisions
    }
}

extension HomeDivisionsListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell  = tableView.dequeueReusableCell(withIdentifier: "reuse", for: indexPath) as? HomeDivisionCell else { return UITableViewCell() }
        
        cell.titleLabel.text = dataSource[indexPath.row].title
        cell.lampSwitch.addTarget(self, action: #selector(handleSwitch), for: .valueChanged)
        
        return cell
    }
    
    @objc func handleSwitch(sender: UISwitch) {
        guard let cell = sender.superview as? HomeDivisionCell,
            let index = tableView.indexPath(for: cell)?.row else { return }
        
        presenter.lampStatusTriggered(index: index, status: sender.isOn)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.height * 0.1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        presenter.homeDivisionTriggered(index: indexPath.row)
    }

}
