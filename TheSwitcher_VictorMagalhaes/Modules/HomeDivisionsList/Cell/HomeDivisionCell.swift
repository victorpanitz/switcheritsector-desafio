//
//  HomeDivisionCell.swift
//  TheSwitcher_VictorMagalhaes
//
//  Created by Victor on 18/12/18.
//  Copyright © 2018 Victor. All rights reserved.
//

import UIKit
import Foundation

class HomeDivisionCell: UITableViewCell {
    
    var titleLabel = UILabel()
    var lampSwitch = UISwitch(frame: .zero)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupTAMIC()
        setupTitle()
        setupLampSwitch()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { return nil }
    
    private func setupTAMIC() {
        [titleLabel, lampSwitch].forEach{
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    private func setupTitle() {
        titleLabel.font = UIFont(name: "Avenir", size: frame.height/3)
        
        addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.heightAnchor.constraint(equalTo: heightAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 40),
            titleLabel.trailingAnchor.constraint(equalTo: centerXAnchor)
            ])
    }
    
    private func setupLampSwitch() {
        addSubview(lampSwitch)
        
        NSLayoutConstraint.activate([
            lampSwitch.centerYAnchor.constraint(equalTo: centerYAnchor),
            lampSwitch.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -40),
            ])
    }
    
}
