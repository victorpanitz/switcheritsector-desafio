
//
//  ViewController.swift
//  TheSwitcher_VictorMagalhaes
//
//  Created by Victor on 18/12/18.
//  Copyright © 2018 Victor. All rights reserved.
//

import UIKit

class HomeDivisionsListRouter: HomeDivisionsListRoutering {

    weak var viewController: UIViewController?
    
    func makeViewController() -> UIViewController {
        let presenter = HomeDivisionsListPresenter(router: self)
        let viewController = HomeDivisionsListViewController(presenter: presenter)
        self.viewController = viewController    
        let navigation = UINavigationController(rootViewController: viewController)
        return navigation
    }
    
    func navigateToHomeDivisionDetails(division: String, status: Bool) {
        let builder = HomeDivisionDetailBuilder(status: status, division: division)
        let nextViewController = builder.makeViewController()
        nextViewController.title = division
        viewController?.navigationController?.pushViewController(nextViewController, animated: true)
    }
}
