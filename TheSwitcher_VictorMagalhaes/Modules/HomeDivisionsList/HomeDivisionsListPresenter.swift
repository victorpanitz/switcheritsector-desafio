//
//  ViewController.swift
//  TheSwitcher_VictorMagalhaes
//
//  Created by Victor on 18/12/18.
//  Copyright © 2018 Victor. All rights reserved.
//

import Foundation

class HomeDivisionsListPresenter {
    
    private let router: HomeDivisionsListRoutering
    private weak var view: HomeDivisionsListView?
    private var divisions: [HomeDivision]?
    
    init(router: HomeDivisionsListRoutering) {
        self.router = router
    }
    
    func attachView(view: HomeDivisionsListView) {
        let hardCodedDimensions: [HomeDivision] = [
            HomeDivision(title: "Kitchen", lampStatus: false),
            HomeDivision(title: "Living Room", lampStatus: false),
            HomeDivision(title: "Master Bedroom", lampStatus: false),
            HomeDivision(title: "Guest's bedroom", lampStatus: false)
        ]
        
        divisions = hardCodedDimensions
        
        view.setHomeDivisions(divisions: hardCodedDimensions)
    }
    
    func homeDivisionTriggered(index: Int) {
        guard let division = divisions?[index] else { return }
        router.navigateToHomeDivisionDetails(
            division: division.title,
            status: division.lampStatus
        )
    }
    
    func lampStatusTriggered(index: Int, status: Bool) {
        divisions?[index].lampStatus = status
    }
    
}
