//
//  HomeDivisionDetailProtocols.swift
//  TheSwitcher_VictorMagalhaes
//
//  Created by Victor Magalhaes on 18/12/18.
//  Copyright © 2018 Victor. All rights reserved.
//

import Foundation

protocol HomeDivisionDetailView: class {
    func setDescription(_ text: String)
    func setStatus(_ text: String)
    func setImage(_ text: String)
}
