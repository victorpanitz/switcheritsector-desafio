//
//  ViewController.swift
//  TheSwitcher_VictorMagalhaes
//
//  Created by Victor on 18/12/18.
//  Copyright © 2018 Victor. All rights reserved.
//

import Foundation

class HomeDivisionDetailPresenter {
    
    private weak var view: HomeDivisionDetailView?
    private let status: Bool
    private let division: String
    
    init(status: Bool, division: String){
        self.status = status
        self.division = division
    }
    
    func attachView(view: HomeDivisionDetailView) {
        view.setDescription("Your \(division) light is")
        switch status {
        case true:
            view.setStatus("ON")
            view.setImage("ic_on")
        case false:
            view.setStatus("OFF")
            view.setImage("ic_off")
        }
    }
}


