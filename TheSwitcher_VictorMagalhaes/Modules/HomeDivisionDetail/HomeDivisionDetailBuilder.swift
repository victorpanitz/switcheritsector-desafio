//
//  File.swift
//  TheSwitcher_VictorMagalhaes
//
//  Created by Victor Magalhaes on 18/12/18.
//  Copyright © 2018 Victor. All rights reserved.
//

import UIKit

class HomeDivisionDetailBuilder {
    
    private let status: Bool
    private let division: String
    
    init(status: Bool, division: String){
        self.status = status
        self.division = division
    }
    
    func makeViewController() -> UIViewController {
        let presenter = HomeDivisionDetailPresenter(status: status, division: division)
        let viewController = HomeDivisionDetailViewController(presenter: presenter)
        
        return viewController
    }

}
