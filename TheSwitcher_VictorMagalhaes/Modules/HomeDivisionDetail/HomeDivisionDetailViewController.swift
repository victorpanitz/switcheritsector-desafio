//
//  HomeDivisionViewController.swift
//  TheSwitcher_VictorMagalhaes
//
//  Created by Victor Magalhaes on 18/12/18.
//  Copyright © 2018 Victor. All rights reserved.
//

import UIKit

class HomeDivisionDetailViewController: UIViewController {
    
    private let lampImageView = UIImageView()
    private let descriptionLabel = UILabel()
    private let statusLabel = UILabel()
    private let presenter: HomeDivisionDetailPresenter
    
    init(presenter: HomeDivisionDetailPresenter) {
        self.presenter = presenter
        
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { return nil }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        setupTAMIC()
        setupLampImageView()
        setupDescriptionLabel()
        setupStatusLabel()
        presenter.attachView(view: self)
    }
    
    private func setupTAMIC() {
        [lampImageView, descriptionLabel, statusLabel].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    private func setupLampImageView() {
        lampImageView.contentMode = .scaleAspectFit
        lampImageView.backgroundColor = #colorLiteral(red: 0.8469662666, green: 0.8471121192, blue: 0.8469570279, alpha: 1)
        view.addSubview(lampImageView)
        
        NSLayoutConstraint.activate([
                lampImageView.topAnchor.constraint(equalTo: view.topAnchor),
                lampImageView.widthAnchor.constraint(equalTo: view.widthAnchor),
                lampImageView.heightAnchor.constraint(equalToConstant: view.frame.height * 0.25)
            ])
    }
    
    private func setupDescriptionLabel() {
        descriptionLabel.textAlignment = .center
        
        view.addSubview(descriptionLabel)

        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: lampImageView.bottomAnchor, constant: view.frame.height*0.02),
            descriptionLabel.widthAnchor.constraint(equalTo: view.widthAnchor),
            descriptionLabel.heightAnchor.constraint(equalToConstant: 40)
            ])
    }
    
    private func setupStatusLabel() {
        statusLabel.textAlignment = .center
        statusLabel.font = UIFont(name: "Avenir-Heavy", size: view.frame.height / 16)
        view.addSubview(statusLabel)

        NSLayoutConstraint.activate([
            statusLabel.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: view.frame.height*0.02),
            statusLabel.widthAnchor.constraint(equalTo: view.widthAnchor),
            statusLabel.heightAnchor.constraint(equalToConstant: view.frame.height / 15.5)
            ])
    }
    
}

extension HomeDivisionDetailViewController: HomeDivisionDetailView {
    
    func setDescription(_ text: String) {
        descriptionLabel.text = text
    }
    
    func setStatus(_ text: String) {
        statusLabel.text = text
    }
    
    func setImage(_ text: String) {
        lampImageView.image = UIImage(named: text)
    }
    
}
