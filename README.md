## Switcher Challange by IT Sector

## Layout

#### iPad Pro 12.9
![alt text](https://i.imgur.com/x4mJ4xK.png)
#### iPad Pro 12.9
![alt text](https://i.imgur.com/fo1IWFm.png)

#### iPhone XR
![alt text](https://i.imgur.com/TdTlJYC.png)
#### iPhone XR
![alt text](https://i.imgur.com/ZLRTkkm.png)


#### iPhone SE
![alt text](https://i.imgur.com/MDHPrVX.png)
#### iPhone SE
![alt text](https://i.imgur.com/y5O3X4I.png)

## Code Coverage
![alt text](https://i.imgur.com/RzBp7i5.png)

## Third party libraries (both of them used in tests target)
https://github.com/Quick/Nimble
https://github.com/Quick/Quick

